This folder contains a description of each model that is stored in the database. The file with the entry point in the model (index.js) and the description of each model are stored here. Each model file describes the data types for each model and the model itself. On this project, you need to add, extend and associate the following database models:
  - admin.model.js - description of the administrator model, the balance of credits of the user platform is also stored here)
  - barmen.model.js - description of the bartender model
  - client.model.js - description of the client model
  - clientAbonement.model.js - description of the subscription model for clients
  - index.model.js - model entry point
  - order.model.js - description of the order model
  - post.model.js - post model description
  - trener.model.js - description of the trainer model
  - promocodes.model.js - description of the promo code model
  - achievement.model.js - description of the achievement model
  - track.model.js - describes the track model

  ---

Эта папка содержит описание каждой модели которые храняться в базе данных. Здесь храниться файл с точкой входа в модели (index.js) а так же описание каждой модели. В каждом файле модели описаны типы данных для кажой модели и сама модель. На данном проекте нужно добавить, продключить и проассоциировать такие модели базы данных:
  - admin.model.js - описание модели администратора так же тут храниться баланс кредитов платформы юзера)
  - barmen.model.js - описание модели бармена
  - client.model.js - описание модели клиента
  - clientAbonement.model.js - описание модели абонимента для клиентов
  - index.model.js - точка входа в модели
  - order.model.js - описание модели заказа
  - post.model.js - описание модели поста
  - trener.model.js - описание модели тренера
  - promocodes.model.js - описание модели промокодов
  - achievement.model.js - описание модели ачивок
  - track.model.js - описывает модель трассы