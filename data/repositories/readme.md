Each file contains queries in the database query language or aggregation framework

 - base.repository.js - accepts a model and describes the most frequent methods that will be executed on all entities,

The rest of the files extend the basic queries for a specific entity

 - admin.repository.js - extends methods for queries to the database for the admin entity
 - barmen.repository.js - extends methods for queries to the base for the bartender entity
 - client.repository.js - extends methods for requests to the base for the client entity
 - clientAbonement.repository.js - extends methods for queries to the base for the subscription entity
 - order.repository.js - extends methods for queries to the base for the order entity
 - post.repository.js - extends methods for queries to the base for the post entity
 - trener.repository.js - extends methods for queries to the base for the trainer entity
 - promocodes.repository.js - extends methods for queries to the base for the entity of promo codes
 - achievement.repository.js - extends methods for queries to the base for the achievement entity
 - track.repository.js - extends methods for queries to the base for the trace entity

---

Каждый файл содержит запросы на языке запросов базы данных или агрегационном фреймворке

 - base.repository.js - принимает модель и описывает самые частые методы которые будут исполняться на всех сущностях,

Остальные файлы расширяют базовые запросы для конкретной сущности

 - admin.repository.js - расширяет методы для для запросов в базу для сущности администратора
 - barmen.repository.js - расширяет методы для для запросов в базу для сущности бармена
 - client.repository.js - расширяет методы для для запросов в базу для сущности клиента
 - clientAbonement.repository.js -  расширяет методы для для запросов в базу для сущности абонемента
 - order.repository.js - расширяет методы для для запросов в базу для сущности заказа
 - post.repository.js - расширяет методы для для запросов в базу для сущности поста
 - trener.repository.js -  расширяет методы для для запросов в базу для сущности тренера
 - promocodes.repository.js -  расширяет методы для для запросов в базу для сущности промокодов
 - achievement.repository.js -  расширяет методы для для запросов в базу для сущности ачивок
 - track.repository.js -  расширяет методы для для запросов в базу для сущности трассы