Contains files with database migrations (i.e. database versions)
Initially contains 1 migration file that creates databases

Each migration contains methods for rolling or rolling back the migration.

It is difficult to describe the final structure of files in this folder, since the base can be expanded with gradual development and there can be a large number of files

---

Содержаться файлы с миграциями базы данных (то-есть версий базы данных)
Изначально содержит 1 файл с миграцией которая создает базы данных

Каждая миграция содержит методы для того чтобы накатить или откатить миграцию

Конечную структуру файлов в данной папке описать сложно, поскольку базу можно расширять с постепенной разработкой и файлов может быть большое количество