This folder is responsible for routes

File structure:

  - index.js - is responsible for the entry point to the controllers, all our controllers will be located here, grouped by entities for which they are responsible

All other files describe the path description for each entity

  - auth.route.js - the route for registering and authorizing users executes business logic to get a user from the user database calls response middleware

  - bar.route.js - Describes routes that allow the following business logic to be executed,
    also, each process calls the corresponding service, which actually helps to reach the database. Routes that should be implemented here
      - Routes for creating / deleting / composing / changing menu assortment
      - The route that describes the business logic for generating PDF documents calls the corresponding helper
      - Route that describes the business logic Logic to get the current menu by the link
      - Route describing QR code generation for menu links
      - Route allowing you to place an order at the bar (both by the bartender and by the client himself)


  - catalog.route.js - Implementation of routes that allow you to keep track of equipment, namely
    change of available equipment for rent (routes are available for employees)
      - adding
      - removal
      - status changes in "availability"
    change catalog for sale
      - adding sell positions
      - deleting sales positions
      - status changes in "availability"


  - order.route.js - Describes routes for the client
      - creating orders
      - changing orders
      - equipment booking

  - payment.route.js - Describes routes for
      - creating promotional codes
      - generating invoices
      - adding / changing / removing payment methods

  - posts.route.js - Implementation of routes that allow blogging, namely
      - getting multiple blog posts (available to all types of users)
      - getting a blog post (available to all types of users)
      - deleting a blog post (available for the admin (check via middleware))
      - adding a blog post (available to everyone except the client (verification via middleware))

  - user.route.js - routes for the user that process any user actions

---

Эта папка отвечает за роуты

Структура файлов: 

  - index.js -- отвечает за точку входа в контролеры здесь будут находиться все наши контроллеры сгрупированые по сущностям за которые они отвечают

Все остальные файлы описывают описание пути для каждой сущности

  - auth.route.js -- роут для регистрации и  авторизации пользователей выполняет бизнес логику для получения пользователя из базы данных пользователя вызывает response middleware

  - bar.route.js -- Описывает роуты, которые позволяют выполнять следующую бизнес логику,
    так же каждый процесс вызывает соответсвующий сервис, который собственно помогает достучаться до базы данных. Роуты которые, должны быть здесь реализованы
      -	Роуты для создания / удаления / составления / изменения ассортимента меню
      - Роут который описывает бизнес логику генерации PDFки  вызывает соответсвующий хелпер
      -	Роут который описывает бизнес логику Логика для получения актуального меню по ссылке
      -	Роут описывающий Генерацию QR кода для ссылки на меню
      -	Роут позволяющий сделать заказ на баре(как барменом, так и самим клиентом)


  - catalog.route.js -- Имплементация роутов, которые позволяют вести учет оборудования, а именно
    изменение доступного оборудования для аренды(роуты доступны для сотрудников)
      - добавление
      - удаление
      - изменения статусв "доступности"
    изменение каталог для продажи
      - добавление позиций на продажу
      - удаление позицис продажи
      - изменения статусв "доступности"


  - order.route.js -- Описывает роуты для клиента
      - создания заказов
      - изменениея заказов
      - бронирование оборудования

  - payment.route.js -- Описывает роуты для
      - создания промокодов
      - генерации инвойсов
      - добавление / изменение / удаление способов оплаты

  - posts.route.js -- Имплементация роутов, которые позволяют вести блог, а именно
      - получение нескольких постов блога(доступна пользователям всех типов)
      - получение поста блога(доступна пользователям всех типов)
      - удаление поста блога(доступна для адмирниратора(проверка через миддлвар))
      - добавление поста блога(доступна для всех кроме клиента(проверка через миддлвар))

  - user.route.js -- роуты для пользователя которые обрабатывают какие - либо действия пользователем

  
