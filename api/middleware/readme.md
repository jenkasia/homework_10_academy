This folder stores intermediate middleware functions that allow you to implement the validation or processing of the request object or the server response object

  - role.middleware.js Middleware which provides an interface for checking access to the application of a specific user
  - autorisarion.middleware.js middleware for processing data during authorization checks the user role
  - error-handler.middleware.js middleware for error handling
  - orderValidator.middleware.js middleware, which provides an interface for checking customer debt and performing other checks that are needed to create an order
  - registrationValidation.js middleware, which provides an interface for checking the required data for registering a user and also allows you to select a service for registering a user by his role
  - response.middleware.js middleware to send response back to client
  - immage.middleware.js middleware uploading a photo or video to a third-party service and returning a link to a photo and video
  - payment.middleware.api.js intermediate functions for working with payment (here hryvnias are converted into credits)

___

В этой папке хранятся промежуточные функции middleware, которые позволяют реализовать валлидацию или обработку объекта запроса или объект ответа сервера

  - role.middleware.js Миддлевар который предоставляет интерфейс для проверки доступа к приложению конкретного пользователя
  - autorisarion.middleware.js миддлвар для обработки данных при авторизации проверяет роль user-а
  - error-handler.middleware.js миддлвар для обработки ошибок
  - orderValidator.middleware.js миддлвар, который предоставляет интерфейс для проверки долга клиента и выполнение других проверок, которые нужны для создания заказа
  - registrationValidation.js  миддлвар, который предоставляет интерфейс для проверки требуемых даных для регистрации пользователя а так же позволяет выбрать сервис для регистрация пользователя по его роли
  - response.middleware.js миддлвар для отправки ответа обратно на клиент
  - immage.middleware.js миддлвар выгрузки фото или видео на сторонний сервис и возврат ссылки на фото и видио
  - payment.middleware.api.js промежуточные функции для работы с оплатой (тут гривны конвертируются в кредиты