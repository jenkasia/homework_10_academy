A folder that contains files that are called by the repository methods to create / update / delete / entities from the database or perform additional data processing, for example, saving data to other services, and after that can also write a link to the database

This project contains the following file structure:
  - auth.service.js call repository methods for authorization and registration

  - bar.service.js repository methods for work with the bar, that is, add / remove / change / etc. records in the database

  - catalog.service.js repository methods for work with the catalog, that is, add / remove / change / etc. records in the database

  - order.service.js repository methods for work with orders, that is, add / remove / change / etc. records in the database

  - posts.route.js repository methods for work with the Blog (posts), that is, add / remove / change / etc. records in the database

  - user.service.js repository methods for work with users, that is, add / remove changes / etc. records in the database

---

Папка которая содержит файлы которые вызываются методы репозитория для создание/ обновления/ удаления/ сущностей из базы данных или выполняют дополнительная обработка данных, например сохранения данных на других сервисах, и после этого могут также записывать ссылку в базу данных 

В данном проекте содержит такую структуру файлов:
  - auth.service.js вызывают методы репозитория для авторизации и регистрации

  - bar.service.js вызываются методы репозитория для работы c баром, то-есть добавления / удаления / изменения / и т.д. записей в базе

  - catalog.service.js вызываются методы репозитория для работы  каталогом, то-есть добавления / удаления / изменения / и т.д. записей в базе

  - order.service.js вызываются методы репозитория для работы с заказами, то-есть добавления / удаления / изменения / и т.д. записей в базе

  - posts.route.js вызываются методы репозитория для работы с Блогом(постами), то-есть добавления / удаления / изменения / и т.д. записей в базе

  - user.service.js вызываются методы репозитория для работы с пользователям, то-есть добавления / удаления изменения / и т.д. записей в базе
