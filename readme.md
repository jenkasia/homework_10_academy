Project structure / Структура проекта
![structure-scheme of project](./schema.png)

For the project, 2 types of data transfer protocols were chosen, namely:
  - WebSocket
    Websockets are taken for the project so that you can notify the user about any action, for example:
      - about successful registration
      - about discounts
      - about special offers
      - about the readiness of the order
      - remind that there is a reservation for a certain day-time
      - notify on receipt of achievement
      - etc
  - Http
    requests are selected to transfer information to the server and client to create requests of different types, for example:
      - request for registration
      - request to create a user
      - inventory reservation request
      - request to change user data
      - a request to receive the current range of offers (whether it is inventory, or something else)
      - request for payment
      - request to get a list of achievements
      - etc

Having studied all the customer's requirements, it became clear that the application would constantly scale.
and so that after a while it was not necessary to rewrite half of the implementation of business logic, it was decided not to use a monolithic architecture, but to give preference to a micro-service one. This is exactly what we need, since each service will be deployed independently and we will not have to control so that when we change something in one place in the code, our application does not break in another place. We can easily split all the logic into modules, each of which will be responsible for solving its own tasks, whether it be user validation through middleware, helper methods for working with other services, or repositories for working with a database. In addition, in a micro-service architecture, it will be easier to refactor the code, because it will be split into files and finding the part that is needed will not pose any problems. In addition, the micro-service architecture makes it easier to work in a team, since everyone can write their own module without waiting for the implementation of other parts of the application from other developers

---
Для проекта были выбраны 2 типа протоколов передачи данных а именно:
  - WebSocket
    Вебсокеты взяты для проекта для того чтобы можно было уведомить пользователя о каком либо действии, например:
      - о успешной регистрации
      - о скидках
      - о специальный предложения
      - о готовности заказа
      - напомнить о том что есть бронь на определенный день-время
      - уведомить о получении ачивки
      - и т.д
  - Http
    запросы выбраны для передачи информации на сервер и клиент для создание запросов разных типов, например:
      - запрос на регистрацию
      - запрос на создание пользователя
      - запрос бронирование инвентаря
      - запрос на изменение данных пользователя
      - запрос на получение текущего ассортимента предложений(будь то инвентарь, или что-то другое)
      - запрос на проведение оплаты
      - запрос на получение списка ачивок
      - и т.д

Изучив все требования заказчика, стало понятно, что приложение буде постоянно масштабироваться
и чтобы через время не пришлось переписывать половину реализации бизнес-логики, било решено не использовать монолитную архитектуру а отдать предпочтение микро-сервисной. Это какраз то что нам нужно, поскольку каждый сервис будет развертываться самостоятельно и нам не прийдет контролировать, чтобы при изменение чего-то в одном месте кода, наше приложение не поломалось в другом месте. Всю логику мы можем запросто разбить на модули, каждый из которых будет отвечать за решение своих задач, будь то валидация пользователя через миддлвары, вспомогательные методы для работы с другими сервисами, или репозитории для работы с базой данных. К тому же в микро-сервисной архитектурой буде проще рефакторить код, потому что он будет разбит на файлы и найти ту часть что нужна, не составит никаких проблем. К тому же микро-сервисная архитектура позволяет облегчить работу в команде, поскольку, каждый может писать свой модуль не дожидаясь реализации других частей приложения от других разработчиков