This folder will store additional "helper" functions that will be available from anywhere in the application, for example
  - Helper providing an interface for creating a random promo code,
   as well as "Personal" promotional code, for example, for an affiliate program
---
В  этой папке будут хранить дополнительные "вспомогательные" функции которые будут доступны из любой точки приложения, например 
 - Хелпер предоставляющий интерфейс для создание случайного промокода,
  а так же "Персонального" промокода, например для партнерской программы
