This folder helps extend the functionality of the application to use not only http requests, but also sockets.
Injector provides an interface for inserting sockets into our application allowing us to call a specific socket after some request

The rest of the files are responsible for this:
  - base.socket.handler.js - description of routes for base sockets that will be used by the system,
  - order.socket.handler.js - notification of the readiness of an order on the bar
  - email.socket.handler.js - description of routes that will send letters to mail
  - notification.socket.handler.js - describes the routes that send notifications to clients, for example, when an achievement has been received or that the lease is about to expire

---
Папка котора помогает расширяет функционал приложения использовать не только http запросы, но еще и сокеты.
Injector предоставляет интерфейс для вставки сокетов в наше приложения позволяющий вызывать определенный сокет после какого - либо запроса

Остальные файлы отвечают за это:
  - base.socket.handler.js - описание роутов для базовых сокетов которые будут использоваться системой,
  - order.socket.handler.js - уведомдение о готовности заказа на баре
  - email.socket.handler.js - описание роутов которые будут отправлять письма на почту
  - notification.socket.handler.js - описывает роуты которые отправляют уведомления клиентам, например о получении ачивки или о том что время аренды подходит к концу