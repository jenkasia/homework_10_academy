Contains configuration files
  - Database
  - third party services that provide API

In this specific task, kinfigs for such APIs will be stored here

  - db.config.js
    - The file contains the read necessary data to connect the database

  - pdfGenerator.js
    - Setting up an api service that allows you to generate PDF docks
    Also here the interface is configured for the possibility of generating this very PDF document

  - qr-code-generator.js
    - Setting up an API service that allows you to generate QR codes
    Also, the interface is configured here to be able to generate this QR code.

  - imgur.config.js
    - Setting up an API service that allows you to generate save photos and videos

  - stripe.config.js
    - Setting up an API service that allows you to make a deposit, also for invoicing payment

  - email.config.js
    - Setting up a service API that allows you to send letters to mail

---

Содержаться файлы с конфигурациями
  - базы данных
  - сторониих сервисов которые предоставляют API

В данном конкретном задании тут буду храниться кинфиги к таким API

  - db.config.js
    - Файл содержит считанные необходимые данные для подключения базы данных

  - pdfGenerator.js
    - Настройка api сервиса который позволяет генерировать PDF доки
    Также тут настраивается интерфейс для возможности генерации этого самого PDF документа

  - qr-code-generator.js
    -  Настройка API сервиса который позволяет генерировать QR коды
    Также тут настраивается интерфейс для возможности генерации этого самого QR кода

  - imgur.config.js
    -  Настройка API сервиса который позволяет генерировать сохранять фото и видио

  - stripe.config.js
    -  Настройка API сервиса который позволяет выполнять депозит, так же для выставления счетов проведения оплаты

  - email.config.js
    -  Настройка API сервиса который позволяет отправлять письма на почту